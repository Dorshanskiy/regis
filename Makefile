BASEDIR = $(shell pwd)
REBAR = rebar3
RELPATH = _build/default/rel/regis
PRODRELPATH = _build/prod/rel/regis
APPNAME = regis
SHELL = /bin/bash

release:
	$(REBAR) release

console:
	cd $(RELPATH) && ./bin/regis console

prod-release:
	$(REBAR) as prod release
	
prod-console:
	cd $(PRODRELPATH) && ./bin/regis console

compile:
	$(REBAR) compile

clean:
	$(REBAR) clean

test:
	$(REBAR) ct

start:
	$(BASEDIR)/$(RELPATH)/bin/$(APPNAME) start

stop:
	$(BASEDIR)/$(RELPATH)/bin/$(APPNAME) stop
