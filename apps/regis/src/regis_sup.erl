%%%-------------------------------------------------------------------
%% @doc regis top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(regis_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

% -define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: #{id => Id, start => {M, F, A}}
%% Optional keys are restart, shutdown, type, modules.
%% Before OTP 18 tuples must be used to specify a child. e.g.
%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
    ChildSpecList = [ child(regis_pnode_s, worker) ],
    SupFlags = #{   strategy    => one_for_one,
                    intensity   => 10,
                    period      => 10
                },
    {ok, { SupFlags, ChildSpecList }}.

%%====================================================================
%% Internal functions
%%====================================================================
child(Module, Type) ->
    #{  id          => Module,
        start       => {Module, start_link, []},
        restart     => temporary,
        shutdown    => 5000,
        type        => Type,
        modules     => [Module]
    }.