-module(regis_pnode_s).
-behaviour(gen_server).

-export([start_link/0, stop/1]).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-record(state, {
    pstore_state :: any(),
    pstore_mod :: any()
}).

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

stop(_Opts) ->
  gen_server:call(?MODULE, stop).


init(_Opts) ->
    process_flag(trap_exit, true),
    PsMod = regis_pstore_ets,
    {ok, PsState} = PsMod:new(),
    {ok, #state{pstore_state=PsState, pstore_mod=PsMod}}.

handle_call(stop, _From, State) ->
    {stop, normal, ok, State}
    ;
handle_call({whereis_job, JobMeta}, _From, State=#state{pstore_state=PsState, pstore_mod=PsMod}) ->
    Res = PsMod:whereis_job(PsState, JobMeta),
    {reply, Res, State}
    ;
handle_call({jobs, JobMeta}, _From, State=#state{pstore_state=PsState, pstore_mod=PsMod}) ->
    Res = PsMod:jobs(PsState, JobMeta),
    {reply, Res, State}
    ;
handle_call({num_registered_pids}, _From, State=#state{pstore_state=PsState, pstore_mod=PsMod}) ->
    Res = PsMod:num_registered_pids(PsState),
    {reply, Res, State}
    ;
handle_call(_Msg, _From, State) ->
    {noreply, State}.


handle_cast({reg, JobMeta, VnodeItem, Pid}, State=#state{pstore_state=PsState, pstore_mod=PsMod}) ->
    PsMod:register(PsState, JobMeta, VnodeItem, Pid),
    {noreply, State}
    ;
handle_cast({unreg, JobMeta, Pid}, State=#state{pstore_state=PsState, pstore_mod=PsMod}) ->
    PsMod:unregister(PsState, JobMeta, Pid),
    {noreply, State}
    ;
handle_cast({unreg_all, Pid}, State=#state{pstore_state=PsState, pstore_mod=PsMod}) ->
    PsMod:unregister_all(PsState, Pid),
    {noreply, State}
    ;
handle_cast(_Msg, State) ->
    {noreply, State}.


handle_info({'EXIT', Pid, _Reason}, State=#state{pstore_state=PsState, pstore_mod=PsMod}) ->
    PsMod:unregister_all(PsState, Pid),
    {noreply, State}
    ;
handle_info(_Info, State) ->
    {noreply, State}.


terminate(_Reason, _State) ->
    ok.


code_change(_OldVsn, State, _Extra) ->
    {ok, State}.